package com.mycompany.inteface;

import com.mycompany.entity.Pedido;
import java.util.List;


public interface IPedidoRepository {
    
    public void add(Pedido pedido);
	
    public void remove(Pedido pedido);

    public List<Pedido> list();

    public void edit(Pedido pedido);
}
