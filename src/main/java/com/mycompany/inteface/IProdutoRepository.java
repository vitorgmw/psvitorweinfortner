package com.mycompany.inteface;

import com.mycompany.entity.Produto;
import java.util.List;

public interface IProdutoRepository {
    
    public void add(Produto produto);
	
    public void remove(Produto produto);

    public List<Produto> list();

    public void edit(Produto produto);
    
}
