package com.mycompany.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import com.mycompany.entity.Produto;
import com.mycompany.inteface.IProdutoRepository;
import com.mycompany.repository.ProdutoRepository;
import java.util.List;
import javax.inject.Inject;

@Controller
public class ProdutoController {
    private final Result result;
	
    protected ProdutoController() {
            this(null);
    }

    @Inject
    public ProdutoController(Result result) {
            this.result = result;
    }

    @Path("/produtos/")
    public void index(){

            List<Produto> produtos = new ProdutoRepository().list();
            result.include("produtos", produtos);

    }

    @Post("/produto/add/")
    public void add(Produto produto){
            IProdutoRepository produtoRepository = new ProdutoRepository();
            produtoRepository.add(produto);
    }

    @Get("/produto/add/")
    public void add(){

    }

    @Path("/produto/edit/{id}")
    public void edit(Integer id){

    }
}
