package com.mycompany.controller;

import java.util.List;
import javax.inject.Inject;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import com.mycompany.entity.Pedido;
import com.mycompany.inteface.IPedidoRepository;
import com.mycompany.repository.PedidoRepository;

@Controller
public class PedidoController {
    private final Result result;
	
    protected PedidoController() {
            this(null);
    }

    @Inject
    public PedidoController(Result result) {
            this.result = result;
    }

    @Path("/pedidos/")
    public void index(){

            List<Pedido> pedidos = new PedidoRepository().list();
            result.include("pedidos", pedidos);

    }

    @Post("/pedido/add/")
    public void add(Pedido pedido){
            IPedidoRepository pedidoRepository = new PedidoRepository();
            pedidoRepository.add(pedido);
    }

    @Get("/pedido/add/")
    public void add(){

    }

    @Path("/pedido/edit/{id}")
    public void edit(Integer id){

    }
}
