package com.mycompany.repository;

import com.mycompany.entity.Pedido;
import com.mycompany.inteface.IPedidoRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class PedidoRepository implements IPedidoRepository{
    EntityManagerFactory emf;
    EntityManager em;

    public PedidoRepository(){
            emf = Persistence.createEntityManagerFactory("psql");
            em = emf.createEntityManager();
    }

    @Override
    public void add(Pedido pedido) {
            try{
                    em.getTransaction().begin();
                    em.persist(pedido);
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();
    }

    @Override
    public void remove(Pedido pedido) {
            try{
                    em.getTransaction().begin();
                    em.remove(pedido);			
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();	
    }


    @Override
    public void edit(Pedido pedido) {
            try{
                    em.getTransaction().begin();
                    em.merge(pedido);
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();
    }

    @Override
    public List<Pedido> list() {
            em.getTransaction().begin();
            Query query = em.createQuery("select pedido from Pedido pedido");
            @SuppressWarnings("unchecked")
            List<Pedido> pedidos = query.getResultList();
            em.getTransaction().commit();
            emf.close();
            return pedidos;
    }
}
