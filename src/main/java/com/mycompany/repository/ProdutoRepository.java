package com.mycompany.repository;

import com.mycompany.entity.Produto;
import com.mycompany.inteface.IProdutoRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class ProdutoRepository implements IProdutoRepository{
    EntityManagerFactory emf;
    EntityManager em;

    public ProdutoRepository(){
            emf = Persistence.createEntityManagerFactory("psql");
            em = emf.createEntityManager();
    }

    @Override
    public void add(Produto produto) {
            try{
                    em.getTransaction().begin();
                    em.persist(produto);
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();
    }

    @Override
    public void remove(Produto produto) {
            try{
                    em.getTransaction().begin();
                    em.remove(produto);			
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();	
    }


    @Override
    public void edit(Produto produto) {
            try{
                    em.getTransaction().begin();
                    em.merge(produto);
                    em.getTransaction().commit();
            }catch(Throwable throwable){
                    em.getTransaction().rollback();
                    throw throwable;
            }
            emf.close();
    }

    @Override
    public List<Produto> list() {
            em.getTransaction().begin();
            Query query = em.createQuery("select produto from Produto produto");
            @SuppressWarnings("unchecked")
            List<Produto> produtos = query.getResultList();
            em.getTransaction().commit();
            emf.close();
            return produtos;
    }
}
