<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <header>
		<h2>Login - Processo Seletivo</h2>
	</header>
        <center>
            <br>
            <form method="POST" action="Servlet">
                <div>
                    <div class="form-group">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario.nome">
                    </div>                
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="text" name="usuario.senha">
                    </div>
                    <input type="submit" value="Entrar" name="login"
                           onclick="javascript:document.location = 'Servlet?ope=index';"> 
                </div>
            </form>
        </center>
    </body>
</html>

