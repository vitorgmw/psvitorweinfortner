<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Pedidos</title>
    </head>
    <body>
        <header>
		<h2>Cadastro de Pedidos - Processo Seletivo</h2>
	</header>
    private long numero;
    private Date data = new Date();
    private String razaoSocial;
    private String cnpj;
    private String telefone;
    private String email;
    
        <form action="Servlet" method="POST">
            <table>
                <tr>
                    <td><h3>Número:</h3></td>
                    <td><input  type="text" name="pedido.numero" disabled="disabled"></td>
                </tr>
                <tr>
                    <td><h3>Data:</h3></td>
                    <td><input type="date" value="" name="pedido.data"></td>
                </tr>
                <tr>
                    <td><h3>Razão Social</h3></td>
                    <td><input required type="text" value="" name="pedido.razaoSocial"></td>
                </tr>
                <tr>
                    <td><h3>CNPJ</h3></td>
                    <td><input required type="text" value="" name="pedido.cnpj"></td>
                </tr>
                <tr>
                    <td><h3>Telefone</h3></td>
                    <td><input required pattern="^\d{5}-\d{4}$" type="tel"
                               value="" name="pedido.telefone"></td>
                </tr>
                <tr>
                    <td><label for="pedido.email"><h3>E-mail</h3></label></td>
                    <td><input required type="email" value="" name="pedido.email"></td>
                </tr>
            </table> 
                
                
            <input type="button" name="index" value="Inicio" 
                   onclick="javascript:document.location='servlet?ope=index';">
            <input type="button" name="salvarconta" value="Salvar"
                   onclick="javascript:document.location='servlet?ope=salvarconta';">
            <input type="button" name="zerardespesa" value="Zerar Ccontas"
                   onclick="javascript:document.location='servlet?ope=zerarcontas';">
        </form>
    </body>
</html>
